// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEscape.h"
#include "OpenDoor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
  // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
  // off to improve performance if you don't need them.
  PrimaryComponentTick.bCanEverTick = true;

  // ...
}

// Called when the game starts
void UOpenDoor::BeginPlay()
{
  Super::BeginPlay();
  Owner = GetOwner();
  if(!PressurePlate)
  {
    UE_LOG(LogTemp, Error, TEXT("%s missing pressure plate assigned"), *GetOwner()->GetName())
  }
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  // Poll the trigger volume every frame
    //if the ActorThatOpens is in the volume
  if (GetTotalMassOfActorsOnPlate() > RequiredMassOnPlateToOpen)
  {
    OnDoorOpen.Broadcast();
  }else
  {
    OnDoorClose.Broadcast();
  }
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
  float TotalMass = 0.f;
  if (PressurePlate)
  {
    TArray<AActor*> OverlappingActors;
    // find all overlapping actors
    PressurePlate->GetOverlappingActors(OUT OverlappingActors);
    // Itterate them through them and add up all masses
    for (auto& actor : OverlappingActors)
    {
      UE_LOG(LogTemp, Warning, TEXT("Found %s on pressure plate"), *actor->GetName())

        TotalMass += actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
    }
  }
  return TotalMass;
}

