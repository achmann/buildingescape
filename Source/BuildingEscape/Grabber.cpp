// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEscape.h"
#include "Grabber.h"

#define OUTVAR
// Sets default values for this component's properties
UGrabber::UGrabber()
{
  // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
  // off to improve performance if you don't need them.

  PrimaryComponentTick.bCanEverTick = true;

  // ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
  Super::BeginPlay();

  Reporting();
  FindPhysicsHandleConponent();
  SetupInputComponent();
}

void UGrabber::FindPhysicsHandleConponent()
{

  /// look for attached physics component handle (Owner has thie grabber and the phys hndl)
  PhysicsHandle = Owner()->FindComponentByClass<UPhysicsHandleComponent>();

  if (PhysicsHandle)
  {

  }
  else
  {
    UE_LOG(LogTemp, Error, TEXT("%s missing physics handle component"), *Owner()->GetName())
  }

}

void UGrabber::SetupInputComponent()
{
  if (Owner())
  {
    /// look for attached input component (only at runtime)
    InputComponent = Owner()->FindComponentByClass<UInputComponent>();

    if (InputComponent)
    {
      if (ReportingEnabled)
      {
        UE_LOG(LogTemp, Warning, TEXT("%s's input component found"), *Owner()->GetName())
      }
      ///Bind the input action
      InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
      ///Bind the input action
      InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);

    }
    else
    {
      UE_LOG(LogTemp, Error, TEXT("%s missing input component"), *Owner()->GetName())
    }
  }
  else
  {
    UE_LOG(LogTemp, Error, TEXT("No Owner assigned"))
  }

}

/// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
  Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

  // If physics handle is attached
  if (PhysicsHandle && PhysicsHandle->GrabbedComponent)
  {
    // move the object that we're holding
    /// Get end of reach of ray trace

    PhysicsHandle->SetTargetLocation(GetEndOfReach());
  }
  /// or do nothing...




}



void UGrabber::Grab() 
{
  if (ReportingEnabled)
  {
    UE_LOG(LogTemp, Warning, TEXT("Grab Pressed"))
  }

  /// LINE TRACE and see if we reach any actors with physics body collision channel set
  auto HitResult = GetFirstPhysicsBodyInReach();

  auto ComponentToGrab = HitResult.GetComponent();
  auto ActorHit = HitResult.GetActor();

  if (ActorHit)
  {
    PhysicsHandle->GrabComponent(
      ComponentToGrab,
      NAME_None, // no bones needed
      ComponentToGrab->GetOwner()->GetActorLocation(),
      true); // allow rotation
  }
}

void UGrabber::Release() 
{
  if (ReportingEnabled)
  {
    UE_LOG(LogTemp, Warning, TEXT("Grab Released"))
  }

  PhysicsHandle->ReleaseComponent();
}



void UGrabber::Reporting()
{
  if (ReportingEnabled)
  {
    FString ObjectName = Owner()->GetName();
    FString ObjectPos = Owner()->GetTransform().GetLocation().ToString();

    UE_LOG(LogTemp, Warning, TEXT("Grabber reporting on %s at position %s"), *ObjectName, *ObjectPos)
  }
}

const FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{

  /// Line trace (Ray-cast) out to reach distance
  FHitResult HitResult;
  /// Setup Query Parameters
  FCollisionQueryParams TraceParameters(FName(TEXT("")), false, Owner());

  GetWorld()->LineTraceSingleByObjectType(
    OUTVAR HitResult,
    GetStartOfReach(),
    GetEndOfReach(),
    FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
    TraceParameters);

  return HitResult;
}

FVector UGrabber::GetEndOfReach()
{
  // Get Player Viewpoint this tick
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
    OUTVAR PlayerViewPointLocation,
    OUTVAR PlayerViewPointRotation);

  return PlayerViewPointLocation + PlayerViewPointRotation.Vector() * Reach;

}


FVector UGrabber::GetStartOfReach()
{
  // Get Player Viewpoint this tick
  GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
    OUTVAR PlayerViewPointLocation,
    OUTVAR PlayerViewPointRotation);

  return PlayerViewPointLocation;

}
