// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

    
    // How far infront of the player can we reach in cm?
    UPROPERTY(EditAnywhere)
    float Reach = 100.f;

    UPROPERTY(EditAnywhere)
    bool ReportingEnabled;
    
private:
    
    AActor* _owner = nullptr;
    FORCEINLINE AActor* Owner(){ if(!_owner) _owner = GetOwner(); return _owner;}
    
    UPhysicsHandleComponent* PhysicsHandle = nullptr;
    
    UInputComponent* InputComponent = nullptr;
    
    FVector PlayerViewPointLocation;
    FRotator PlayerViewPointRotation;
    
    void Reporting();
    
	// Find (assumed) attached physics handle
    void FindPhysicsHandleConponent();
    
    // Setup (Assumed) attached input component
    void SetupInputComponent();
    
    // Raycast and grab what's in read
    void Grab();
    // Release what's grabbed
    void Release();
    
    // Return hit for first physics body in reach
    const FHitResult GetFirstPhysicsBodyInReach();
    
    // Returns current start of reach (usually, when grabbing)
    FVector GetStartOfReach();
    
    // Returns current end of reach (usually, when grabbing)
    FVector GetEndOfReach();

    
};
